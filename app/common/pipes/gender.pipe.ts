import { Pipe, PipeTransform } from '@angular/core';
import { Gender } from "../../models/patient.model";

@Pipe({ name: 'gender' })
export class GenderPipe implements PipeTransform {

    transform(value: Gender): string {
        return value === Gender.male ? 'Male' : 'Female';
    }

}