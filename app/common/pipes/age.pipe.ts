import { Pipe, PipeTransform } from '@angular/core';
import * as moment from 'moment/moment';

@Pipe({ name: 'age' })
export class AgePipe implements PipeTransform {

    transform(value: Date): number {
        return moment().diff(moment(value, "DD-MM-YYYY"), 'years');
    }

}