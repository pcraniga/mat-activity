import { Injectable } from '@angular/core';
import { Observable } from 'rxjs/RX';
import { Http, Response } from '@angular/http';
import { Intensity } from "../../models/activity.model";

@Injectable()
export class DefinitionsService {

    constructor(private http: Http) {

    }

    // Retrieves activity definitions
    getActivityDefinitions(): Observable<any[]> {
        return this.http.get('./app/api/definitions/activities.json').map((response: Response) => {
            return response.json().map(a => {
                a.intensity = Intensity[a.intensity];
                return a;
            });
        }).catch(this.handleError);
    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }
}