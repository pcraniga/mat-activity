import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { DefinitionsService } from "./definitions.service";

@Injectable()
export class ActivityDefinitionsResolver implements Resolve<any> {
    constructor(private definitionsService: DefinitionsService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.definitionsService.getActivityDefinitions();
    }
}