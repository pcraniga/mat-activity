import { Component, Input } from '@angular/core';

@Component({
    templateUrl: 'app/common/errors/page-not-found.component.html',
    styleUrls: ['app/common/errors/page-not-found.component.css']
})
export class PageNotFoundComponent {

}