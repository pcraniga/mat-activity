import { Routes } from '@angular/router';
import { PageNotFoundComponent } from './common/errors/page-not-found.component';

import {
    PatientListComponent,
    PatientDetailComponent,
    PatientResolver,
    PatientListResolver
} from './patients/index';
import { ActivityDefinitionsResolver } from "./common/index";

export const appRoutes: Routes = [
    {
        path: 'patients', component: PatientListComponent,
        resolve: {
            patients: PatientListResolver,
            definitions: ActivityDefinitionsResolver
        }
    },
    {
        path: 'patients/:id', component: PatientDetailComponent,
        resolve: {
            patient: PatientResolver,
            definitions: ActivityDefinitionsResolver
        }
    },
    { path: '', redirectTo: '/patients', pathMatch: 'full' },
    { path: '**', component: PageNotFoundComponent }
];