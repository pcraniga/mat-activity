import { Injectable } from '@angular/core';
import { Subject, Observable } from 'rxjs/RX';
import { IPatient, Gender } from "../models/patient.model";
import { IActivity, Intensity } from "../models/activity.model";
import { Http, Response } from '@angular/http';

@Injectable()
export class PatientsService {

    constructor(private http: Http) {

    }

    // Retrieves a collection of patients
    getPatients(): Observable<IPatient[]> {
        return this.http.get('./app/api/patients.json').map((response: Response) => {
            return this.getPatientsFromResponse(response);
        }).catch(this.handleError);
    }

    // Retrieves a patient with the specified ID.
    getPatientById(id: number): Observable<IPatient> {
        return this.http.get('./app/api/patients.json').map((response: Response) => {
            let patients: IPatient[] = this.getPatientsFromResponse(response);
            return patients.find(p => p.id === id);
        }).catch(this.handleError);
    }

    getActivitiesForPatient(patientId: number): Observable<IActivity[]> {
        return this.http.get(`./app/api/patients/${patientId}/summary.json`).map((response: Response) => {
            return response.json();
        });
    }

    getActivityDurationDetails(patient: IPatient): any {
        let isBelow = false;
        let moderateDuration = 0;
        let vigorousDuration = 0;
        let activeDuration = 0;


        if (patient && patient.activities) {
            let activities = patient.activities;

            moderateDuration = this.getActivityDuration(activities, Intensity.moderate);
            vigorousDuration = this.getActivityDuration(activities, Intensity.vigorous);
            activeDuration = vigorousDuration * 2 + moderateDuration;

            if (moderateDuration < 150 &&
                vigorousDuration < 75 &&
                activeDuration < 150) {
                isBelow = true;
            }
        }

        return {
            isBelow,
            moderateDuration,
            vigorousDuration,
            activeDuration
        }
    }

    getActivityDuration(activities: IActivity[], intensity: Intensity) {
        let duration = 0;

        if (activities && activities.length > 0) {
            duration = activities.reduce((acc, curr) => {
                if (curr.intensity === intensity) {
                    acc += curr.minutes;
                }
                return acc;
            }, 0);
        }

        return duration;
    }

    private getPatientsFromResponse(response: Response): IPatient[] {
        let patients: IPatient[] = response.json().map(p => {
            p.birthDate = new Date(p.birthDate);
            p.gender = Gender[p.gender];

            this.getActivitiesForPatient(p.id).subscribe(a => {
                p.activities = a;
            });

            return p;
        });

        return patients;
    }

    private handleError(error: Response) {
        return Observable.throw(error.statusText);
    }
}