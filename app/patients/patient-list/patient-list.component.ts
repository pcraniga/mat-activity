import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { IPatient } from "../../models/patient.model";
import { IActivity, Intensity } from "../../models/activity.model";
import * as moment from 'moment/moment';
import { PatientsService } from "../patients.service";

@Component({
    templateUrl: 'app/patients/patient-list/patient-list.component.html',
    styleUrls: ['app/patients/patient-list/patient-list.component.css']
})
export class PatientListComponent implements OnInit {
    patients: IPatient[];
    options: Object;

    constructor(private route: ActivatedRoute, private patientsService: PatientsService) {

    }

    ngOnInit(): void {
        let patients: IPatient[] = this.route.snapshot.data['patients'];
        let definitions: any[] = this.route.snapshot.data['definitions'];

        patients.map(p => {
            p.activities.map(a => {
                a.intensity = Intensity.none;
                let activity = definitions.find(d => d.activity === a.activity);
                if (activity) {
                    a.intensity = activity.intensity;
                }
            });

            let { isBelow, activeDuration } = this.patientsService.getActivityDurationDetails(p);
            p.isBelowThreshold = isBelow;
            p.activeMinutes = activeDuration;
        });

        this.patients = patients;
        this.sortBy('duration');

        this.generateChart();
    }

    sortBy(fieldName: string) {
        if (fieldName === 'name') {
            this.patients.sort(sortByNameAsc);
        } else if (fieldName === 'age') {
            this.patients.sort(sortByAgeAsc);
        } else if (fieldName === 'duration') {
            this.patients.sort(sortByDuration);
        } else {
            this.patients.sort(sortByIdAsc);
        }
    }

    generateChart() {
        if (!this.patients && this.patients.length === 0) return;
        let patientNames = this.patients.map(p => p.name);
        let series = [];

        for (let item in Intensity) {
            if (isNaN(Number(item))) {

                let data = [];
                this.patients.forEach(p => {
                    let matchingActivities = p.activities.filter(a => Intensity[a.intensity].toString() === item);
                    if (matchingActivities && matchingActivities.length > 0) {
                        let activityTotal = matchingActivities.reduce((acc, curr) => {
                            return acc + curr.minutes;
                        }, 0);
                        data.push(activityTotal);
                    } else {
                        data.push(0);
                    }
                });

                series.push({
                    name: item,
                    data
                });
            }
        }

        this.options = {
            chart: { type: 'column' },
            title: { text: 'Activity Level By Patient' },
            xAxis: {
                categories: patientNames
            },
            yAxis: {
                min: 0,
                title: {
                    text: 'Total Activity'
                },
                stackLabels: {
                    enabled: true,
                    style: {
                        fontWeight: 'bold',
                        color: 'gray'
                    }
                }
            },
            legend: {
                align: 'right',
                x: -30,
                verticalAlign: 'top',
                y: 25,
                floating: true,
                backgroundColor: 'white',
                borderColor: '#CCC',
                borderWidth: 1,
                shadow: false
            },
            plotOptions: {
                column: {
                    stacking: 'normal',
                    dataLabels: {
                        enabled: true,
                        color: 'white'
                    }
                }
            },
            series
        };
    }
}

function sortByNameAsc(p1: IPatient, p2: IPatient) {
    if (p1.name > p2.name) return 1;
    else if (p1.name === p2.name) return 0;
    else return -1;
}

function sortByAgeAsc(p1: IPatient, p2: IPatient) {
    const p1_age = moment().diff(moment(p1.birthDate, "DD-MM-YYYY"), 'years');
    const p2_age = moment().diff(moment(p2.birthDate, "DD-MM-YYYY"), 'years');
    return sortNumAsc(p1_age, p2_age);
}

function sortByDuration(p1: IPatient, p2: IPatient) {
    return sortNumAsc(p1.activeMinutes, p2.activeMinutes);
}

function sortByIdAsc(p1: IPatient, p2: IPatient) {
    return sortNumAsc(p1.id, p2.id);
}

function sortNumAsc(n1: number, n2: number) {
    if (n1 > n2) return 1;
    else if (n1 === n2) return 0;
    else return -1;
}