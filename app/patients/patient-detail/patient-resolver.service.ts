import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { PatientsService } from "../patients.service";
import { IPatient } from "../../models/patient.model";

@Injectable()
export class PatientResolver implements Resolve<IPatient> {
    constructor(private patientsService: PatientsService) {

    }

    resolve(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) {
        return this.patientsService.getPatientById(+route.params['id']);
    }
}