import { Component, OnInit } from '@angular/core';
import { PatientsService } from '../patients.service';
import { ActivatedRoute } from '@angular/router';
import { IPatient } from "../../models/patient.model";
import { Intensity } from "../../models/activity.model";

@Component({
    templateUrl: 'app/patients/patient-detail/patient-detail.component.html',
    styleUrls: ['app/patients/patient-detail/patient-detail.component.css']
})
export class PatientDetailComponent implements OnInit {
    patient: IPatient;

    constructor(private patientsService: PatientsService,
        private route: ActivatedRoute) {

    }

    ngOnInit(): void {
        let patient = this.route.snapshot.data['patient'];
        let definitions = this.route.snapshot.data['definitions'];

        if (patient && patient.activities) {
            patient.activities.map(a => {
                a.intensity = Intensity.none;
                let activity = definitions.find(d => d.activity === a.activity);
                if (activity) {
                    a.intensity = activity.intensity;
                }
            });
        }

        this.patient = patient;
    }
}