import { IActivity } from "./activity.model";

export interface IPatient {
    id: number,
    name: string,
    gender: Gender,
    birthDate: Date,
    heightCm: number,
    weightKg: number,
    bmi: number,
    isBelowThreshold: Boolean,
    activeMinutes: number,
    activities?: IActivity[]
}

export enum Gender { male, female };