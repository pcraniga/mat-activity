export interface IActivity {
    activity: string,
    intensity: Intensity,
    minutes: number
}

export enum Intensity {
    none,
    low,
    moderate,
    vigorous
};