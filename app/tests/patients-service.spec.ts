import { PatientsService } from '../patients/patients.service';
import { Observable } from 'rxjs/RX';
import { IPatient, Gender } from "../models/patient.model";
import { Intensity } from "../models/activity.model";

describe('PatientsService', () => {
    let patientsService: PatientsService;
    let mockHttp;

    beforeEach(() => {
        // Not used, but this is how we could mock dependencies
        jasmine.createSpyObj('mockHttp', ['get']);
        patientsService = new PatientsService(mockHttp);
    })

    describe('getActivityDurationDetails', () => {
        it('should return activity durations and threshold', () => {
            let patient: IPatient = {
                id: 1,
                name: "John Smith",
                gender: Gender.male,
                birthDate: new Date("1980/1/1"),
                heightCm: 400,
                weightKg: 50,
                bmi: 40,
                isBelowThreshold: false,
                activeMinutes: 0,
                activities: [
                    {
                        activity: "sleeping",
                        intensity: Intensity.none,
                        minutes: 1440
                    }
                ]
            };

            let details = patientsService.getActivityDurationDetails(patient);

            it('should be below threshold', () => {
                expect(details.isBelow).toBe(true);
            });

            it('should not have moderate activity', () => {
                expect(details.moderateDuration).toBe(0);
            });

            it('should not have vigorous activity', () => {
                expect(details.vigorousDuration).toBe(0);
            });

            it('should not have any activity', () => {
                expect(details.activeDuration).toBe(0);
            });
        });
    });

});