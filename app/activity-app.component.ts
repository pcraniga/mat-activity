import { Component } from '@angular/core';

@Component({
    selector: 'activity-app',
    template: `
        <app-header></app-header>
        <router-outlet></router-outlet>
        `
})
export class ActivityAppComponent {

}