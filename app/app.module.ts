import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { RouterModule } from '@angular/router';
import { HttpModule } from '@angular/http';
import { HashLocationStrategy, LocationStrategy } from '@angular/common';
import { ChartModule } from 'angular2-highcharts';

import {
    HeaderComponent,
    PageNotFoundComponent,
    GenderPipe,
    AgePipe,
    DefinitionsService,
    ActivityDefinitionsResolver
} from "./common/index";

import {
    PatientListComponent,
    PatientDetailComponent,
    PatientsService,
    PatientResolver,
    PatientListResolver
} from './patients/index';

import { ActivityAppComponent } from './activity-app.component';
import { appRoutes } from './routes';

@NgModule({
    imports: [
        BrowserModule,
        HttpModule,
        RouterModule.forRoot(appRoutes),
        ChartModule.forRoot(require('highcharts'))
    ],
    declarations: [
        ActivityAppComponent,
        PatientListComponent,
        PatientDetailComponent,
        HeaderComponent,
        PageNotFoundComponent,
        GenderPipe,
        AgePipe
    ],
    providers: [
        PatientsService,
        PatientResolver,
        PatientListResolver,
        DefinitionsService,
        ActivityDefinitionsResolver,
        { provide: LocationStrategy, useClass: HashLocationStrategy }
    ],
    bootstrap: [ActivityAppComponent]
})
export class AppModule { }